//Document Object Model — это обьектная модель документа, которая позволяет джава скрипту получить доступ к содержимому HTML.

function toCreateList(array) {
    let ul = document.createElement('ul');
    document.body.append(ul);

    let newArray = array.map(function (item) {
        let li = document.createElement('li');
        ul.append(li);
        return li.innerHTML = `данные массива ` + item;
    });
}

let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

toCreateList(arr);